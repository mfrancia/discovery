# README #

Discover an active application over LAN or remote network (using a RESTful DNS like web application). Remove *dead application* using Heart bit mechanism.

Developed in Scala as an integration of University project [MoK reactions: state-of-art self-* algorithms for coordination (Matteo Francia, Giovanni Ciatto)](http://apice.unibo.it/xwiki/bin/view/MoK/Projects). UniBO credentials are required to access to the documentation. [See **discovery** in action.](https://www.youtube.com/watch?v=Fg7OeAE-pvM)
 

