var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
//BSON = mongo.BSONPure;
    BSON = require('bson').BSONPure,
    debug = false;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('tuplecenterdb', server);

function log(log) {
    if (debug) console.log(log);
}

db.open(function (err, db) {
    if (!err) {
        log("Connected to 'tuplecenterdb' database");
        db.collection('tuplecenters', {strict: true}, function (err, collection) {
            if (err) {
                log("The 'tuplecenters' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
    }
});

exports.search = function (req, res) {
    var topic = req.params.topic, id = req.params._id, location = req.params.location;
    console.log('Retrieving tuplecenters: ' + id + ' ' + topic + ' ' + location);
    db.collection('tuplecenters', function (err, collection) {
        collection.find(/* tc.id != id && (tc.topic == topic || tc.location == location) */
//            {$and:
//                [
//                    {'_id':{$ne:id}},
//            {
//                $or: [
//                    {'_topic': topic},
//                    {'_location': location}
//                ]
//            }
//                ]
//            }
        ).toArray(function (err, item) {
                res.send(item);
            });
    });
};

exports.findAll = function (req, res) {
    db.collection('tuplecenters', function (err, collection) {
        collection.find().toArray(function (err, items) {
            res.send(items);
        });
    });
};

exports.addTupleCenter = function (req, res) {
    // the "post" body contains RESTFUL information about the tuplecenter
    var id = req.params.id;
    var tuplecenter = req.body;

    db.collection('tuplecenters', function (err, collection) {
        collection.find({'_id': id /*new BSON.ObjectID(id) */}).limit(1).toArray(function (err, items) {
            if (items.length == 1) {
                log('Updating tuplecenter: ' + JSON.stringify(tuplecenter));
                /* items already exists, update it */
                var id = req.params.id; // to update I need also an id
                collection.update({'_id': id /*new BSON.ObjectID(id)*/}, tuplecenter, {safe: true}, function (err, result) {
                    if (err) {
                        log('Error updating tuplecenter: ' + err);
                        res.send({'error': 'An error has occurred'});
                    } else {
                        log('' + id + ' updated');
                        res.send(req.ip);
                    }
                });
            } else {
                log('Adding tuplecenter: ' + JSON.stringify(tuplecenter));
                /* items doesn't exist, add it */
                collection.insert(tuplecenter, {safe: true}, function (err, result) {
                    if (err) {
                        res.send({'error': 'An error has occurred'});
                    } else {
                        log('Success: ' + JSON.stringify(result.insertedIds[0]));
                        res.send(req.ip);
                    }
                });
            }
        });
    });
};

exports.deleteAllTupleCenter = function (req, res) {
    log('Deleting all tuplecenters');
    db.collection('tuplecenters', function (err, collection) {
        collection.remove({}, {safe: true}, function (err, result) {
            if (err) {
                res.send({'error': 'An error has occurred - ' + err});
            } else {
                log('documents deleted');
                res.send(req.body);
            }
        });
    });
};

exports.deleteOldTupleCenter = function (req, res) {
    var filtermillis = req.params.time;
    log('Deleting tuplecenters older than ' + filtermillis + 'ms');
    db.collection('tuplecenters', function (err, collection) {
        var date = Date.now() - filtermillis;
        log(date + '');
        collection.remove({'_timestamp': {$lt: date}}, {safe: true}, function (err, result) {
            if (err) {
                res.send({'error': 'An error has occurred - ' + err});
            } else {
                log('old tuple centers deleted');
                res.send(result);
            }
        });
    });
};

/*--------------------------------------------------------------------------------------------------------------------*/
// Populate database with sample data -- Only used once: the first time the application is started.
// You'd typically not find this code in a real-life app, since the database would already exist.
var populateDB = function () {
    var tuplecenters = [
        {
            name: "a",
            address: "x.y.w.z",
            port: "20504",
            topic: "blablabla",
            location: "bielefeld",
            timestamp: "0"
        },
        {
            name: "b",
            address: "x.y.w.z",
            port: "20504",
            topic: "bla",
            location: "cesena",
            timestamp: "0"
        }
    ];
    db.collection('tuplecenters', function (err, collection) {
        collection.insert(tuplecenters, {safe: true}, function (err, result) {
        });
    });

};