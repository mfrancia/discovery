package it.unibo.mfrancia.discovery

/**
 * Daemons linked to the dns server, every "cleanMillis" delete the tuple
 * older than "cleanMillis"
 * @param server server addr
 * @param cleanMillis
 */
class Daemon_Cleaner_Server(server: String, cleanMillis: Long) extends Daemon("Daemon_Cleaner_Server") {
  def this(cleanMillis: Long) = this(Syskb.DNSSERVER_TUPLECENTER, cleanMillis)

  def this() = this(Syskb.DNSSERVER_TUPLECENTER, Syskb.DNSSERVER_CLEAN_MILLIS)

  override def run() = {
    val http = new MyHttp(server + cleanMillis.toString)
    while (!isStopped) {
      /* delete older than cleanMillis*/
      println("cleaning server")
      println(http.sendDelete)
      Thread.sleep(cleanMillis)
    }
  }
}

object Daemon_Cleaner_Server extends App {
  new Thread(new Daemon_Cleaner_Server(Syskb.DNSSERVER_CLEAN_MILLIS)).start
}
