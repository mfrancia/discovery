package it.unibo.mfrancia.discovery

/**
 * Created by w4bo on 23/10/15.
 */
abstract class Serializable() {
  def getId: String = id
  def id: String
  def toJson: String
}
