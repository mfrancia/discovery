package it.unibo.mfrancia.discovery

import java.net._

import scala.collection.mutable

class Daemon_TCNeighbours_LAN(tc: mutable.Buffer[iTupleCenter]) extends Daemon_TCNeighbours("Daemon_TCNeighbours_LAN", tc) {
  val serverSocket: DatagramSocket = new DatagramSocket(Syskb.LOCALDAEMON_UDP_PORT)
  val receiveData: Array[Byte] = new Array[Byte](Syskb.LOCALDAEMON_UDP_BUFSIZE)
  val receivePacket: DatagramPacket = new DatagramPacket(receiveData, receiveData.length)

  def this() = this(mutable.ListBuffer.empty)

  override def run() =
    while (!isStopped) {
      /* waiting for the new tuple center */
      val ntc = getNeighbour()
      /* add the new tuple center to existing ones */
      tc.foreach(t => t.addNeighbour(ntc))
    }

  def getNeighbour(): iTupleCenter = {
    /* waiting for a ping */
    serverSocket.receive(receivePacket)
    /* ping received */
    val tc: String = new String(receivePacket.getData().take(receivePacket.getLength))
    println("[Daemon_TCNeighbours_LAN]: " + tc)
    /* deserialize the tuple center */
    val neighbour = TupleCenter.fromJson(tc)
    var address = receivePacket.getAddress.toString
    if (address.charAt(0) == '/') address = address.substring(1)
    neighbour.address = address

    neighbour
  }
}
