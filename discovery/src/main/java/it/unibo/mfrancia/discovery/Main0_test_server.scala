package it.unibo.mfrancia.discovery

/**
 * Created by w4bo on 02/11/15.
 */
object Main0_test_server extends App {
  val http = new MyHttp[TupleCenter](Syskb.DNSSERVER_TUPLECENTER)

  println("\nbefore deleting")
  println(http.sendGet)

  println("\ndeleting, res: " + http.sendDelete(Syskb.DNSSERVER_TUPLECENTER))

  println("\nget: is it ok?? should be []")
  println(http.sendGet)

  val tc = new TupleCenter("a", "x.y.x.z", 20504, "bla", "bielefeld")
  tc.address = http.sendPost(Syskb.DNSSERVER_PING(tc.id), tc)
  println("\nposting, res: " + tc.address)

  tc.updateTimeStamp
  println("\nupdating the center, res: " + http.sendPost(Syskb.DNSSERVER_PING(tc.id), tc, tc.getFields))

  println("\nget: ...is it ok??")
  println(http.sendGet)


  tc.updateTimeStamp
  println("\nupdating the center, res: " + http.sendPost(Syskb.DNSSERVER_PING(tc.id), tc, tc.getFields))

  println("\nget: ...is it ok??")
  println(http.sendGet)
}
