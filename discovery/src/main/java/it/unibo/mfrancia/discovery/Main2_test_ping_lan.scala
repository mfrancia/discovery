package it.unibo.mfrancia.discovery

import java.util.concurrent.{ExecutorService, Executors}

import scala.collection.mutable

/**
 * Created by w4bo on 02/11/15.
 */
object Main2_test_ping_lan extends App {
  /* create tuple centers e pinger init */
  val tc1: iTupleCenter = new TupleCenter("a", "localhost", 20504, "bla", "bielefeld")
  val tc1_pinger = new Daemon_TCPing_Lan(tc1, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc2 = new TupleCenter("b", "localhost", 20504, "bla", "munster")
  val tc2_pinger = new Daemon_TCPing_Lan(tc2, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc3 = new TupleCenter("c", "127.0.0.1", 20504, "blablabla", "berlin")
  val tc3_pinger = new Daemon_TCPing_Lan(tc3, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc4 = new TupleCenter("d", "127.0.0.1", 20504, "blablablabla", "bielefeld")
  val tc4_pinger = new Daemon_TCPing_Lan(tc4, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc5 = new TupleCenter("e", "127.0.0.1", 20504, "a.*", "-")
  val tc5_pinger = new Daemon_TCPing_Lan(tc5, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc6 = new TupleCenter("f", "127.0.0.1", 20504, "a.b.*", "--")
  val tc6_pinger = new Daemon_TCPing_Lan(tc6, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)
  val tc7 = new TupleCenter("g", "127.0.0.1", 20504, "*", "---")
  val tc7_pinger = new Daemon_TCPing_Lan(tc7, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)

  /* tcneight init */
  val tc_list = mutable.ListBuffer(tc1, tc2, tc3, tc4, tc5, tc6, tc7)
  val tcneigh = new Daemon_TCNeighbours_LAN(tc_list)

  val pool: ExecutorService = Executors.newFixedThreadPool(8)
  pool.execute(tcneigh)
  pool.execute(tc1_pinger)
  pool.execute(tc2_pinger)
  pool.execute(tc3_pinger)
  pool.execute(tc4_pinger)
  pool.execute(tc5_pinger)
  pool.execute(tc6_pinger)
  pool.execute(tc7_pinger)
  System.in.read
  tcneigh.setStop
  tc1_pinger.setStop
  tc2_pinger.setStop
  tc3_pinger.setStop
  tc4_pinger.setStop
  tc5_pinger.setStop
  tc6_pinger.setStop
  tc7_pinger.setStop
  System.exit(0)
}
