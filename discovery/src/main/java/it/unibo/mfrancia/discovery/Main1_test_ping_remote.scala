package it.unibo.mfrancia.discovery

import java.util.concurrent.{ExecutorService, Executors}

import scala.collection.mutable

/**
 * Created by w4bo on 02/11/15.
 */
object Main1_test_ping_remote extends App {
  val http = new MyHttp[iTupleCenter](Syskb.DNSSERVER_TUPLECENTER)
  println("cleaning database: " + http.sendDelete)
  /* create tuple centers e pinger init */
  val tc1: iTupleCenter = new TupleCenter("a", "localhost", 20504, "bla", "bielefeld")
  val tc2 = new TupleCenter("b", "localhost", 20504, "bla", "munster")
  val tc3 = new TupleCenter("c", "127.0.0.1", 20504, "blablabla", "berlin")
  val tc4 = new TupleCenter("d", "127.0.0.1", 20504, "blablablabla", "bielefeld")
  val tc5 = new TupleCenter("e", "127.0.0.1", 20504, "a.*", "-")
  val tc6 = new TupleCenter("f", "127.0.0.1", 20504, "a.b.*", "--")
  val tc7 = new TupleCenter("g", "127.0.0.1", 20504, "*", "---")
  val tc1_pinger = new Daemon_TCPing_Remote(tc1, 200, Syskb.DNSSERVER_RETRIES)
  val tc2_pinger = new Daemon_TCPing_Remote(tc2, 200, Syskb.DNSSERVER_RETRIES)
  val tc3_pinger = new Daemon_TCPing_Remote(tc3, 4000, Syskb.DNSSERVER_RETRIES)
  val tc4_pinger = new Daemon_TCPing_Remote(tc4, 3500, Syskb.DNSSERVER_RETRIES)
  val tc5_pinger = new Daemon_TCPing_Remote(tc5, 200, Syskb.DNSSERVER_RETRIES)
  val tc6_pinger = new Daemon_TCPing_Remote(tc6, 200, Syskb.DNSSERVER_RETRIES)
  val tc7_pinger = new Daemon_TCPing_Remote(tc7, 200, Syskb.DNSSERVER_RETRIES)
  /* tcneight init */
  val tc_list = mutable.ListBuffer(tc1, tc2, tc3, tc4, tc5, tc6, tc7)
  val tcneigh = new Daemon_TCNeighbours_Remote(tc_list, Syskb.DNSSERVER_RETRIEVE_MILLIS)
  val cleaner = new Daemon_Cleaner_Server(Syskb.DNSSERVER_TUPLECENTER, 1000)

  val pool: ExecutorService = Executors.newFixedThreadPool(9)
  pool.execute(tcneigh)
  pool.execute(cleaner)
  pool.execute(tc1_pinger)
  pool.execute(tc2_pinger)
  pool.execute(tc3_pinger)
  pool.execute(tc4_pinger)
  pool.execute(tc5_pinger)
  pool.execute(tc6_pinger)
  pool.execute(tc7_pinger)

  System.in.read
  tcneigh.setStop
  tc1_pinger.setStop
  tc2_pinger.setStop
  tc3_pinger.setStop
  tc4_pinger.setStop
  tc5_pinger.setStop
  tc6_pinger.setStop
  tc7_pinger.setStop

  System.in.read
  cleaner.setStop
}
