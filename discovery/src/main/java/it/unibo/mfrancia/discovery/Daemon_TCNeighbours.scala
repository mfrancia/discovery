package it.unibo.mfrancia.discovery

import scala.collection.mutable

/**
 * This daemons gets the lists of neighbours of the tuple center.
 * @param tc specified tuple center
 */
abstract class Daemon_TCNeighbours(id: String, tc: mutable.Buffer[ _>: iTupleCenter]) extends Daemon(id) {
  def addNeighbours(ntc: TupleCenter) = tc += ntc

  def removeNeighbours(ntc: TupleCenter) = tc -= ntc
}