package it.unibo.mfrancia.discovery

import scala.collection.mutable

abstract class Daemon_Cleaner_Local(tcs: mutable.ArrayBuffer[iTupleCenter], cleanMillis: Long) extends Daemon("Daemon_Cleaner_Local") {
  def clean(tc: iTupleCenter)

  def addTupleCenter(tc: iTupleCenter) = tcs += tc
  def removeTupleCenter(tc: iTupleCenter) = tcs -= tc

  override def run() = {
    while (!isStopped) {
      tcs.foreach(tc => clean(tc))
      Thread.sleep(cleanMillis)
    }
  }
}
