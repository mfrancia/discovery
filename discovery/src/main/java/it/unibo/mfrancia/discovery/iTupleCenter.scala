package it.unibo.mfrancia.discovery

import java.util.UUID

import com.google.gson.Gson

/**
  * Created by w4bo on 24/11/15.
  */
trait iTupleCenter extends Serializable {
  def getIdJson: (String, String) = ("_id", id)

  def getFields: Seq[(String, String)] =
    Array(("name", name), ("address", address), ("port", port.toString), ("topic", topic), ("location", location), ("timestamp", timestamp.toString))

  override def toJson = new Gson().toJson(new TupleCenter(id, name, address, port, topic, location, timestamp) /*this.asInstanceOf[TupleCenter]*/).stripMargin

  override def toString = s"$name@$address:$port"

  def name: String

  def address: String

  def address_=(address: String)

  def topic: String

  def topic_=(topic: String)

  def timestamp: Long

  def port: Int

  def location: String

  def location_=(location: String)

  override def id: String

  def addNeighbour(tc: Seq[iTupleCenter]): Unit = tc.foreach(t => addNeighbour(t))

  def addNeighbour(tc: iTupleCenter): Unit =
    if (!tc.id.equals(id)) {
      if (neighborCriteria(tc.topic, tc.location)) {
        addOther(tc)
        println(s"[$toString] added ${tc.toString}")
      } else rejectOther(tc)
    } else addSelf(tc)

  def topicCriteria(topic: String): Boolean

  def locationCriteria(location: String): Boolean

  def neighborCriteria(topic: String, location: String): Boolean

  def rejectOther(tc: iTupleCenter)

  def addOther(tc: iTupleCenter)

  def addSelf(tc: iTupleCenter)

  def updateTimeStamp(): Unit
}
