package it.unibo.mfrancia.discovery

import java.util.UUID

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
  * Tuple center abstraction
  * @param _id tuple center unique id (never change over time)
  * @param _name tuple center name
  * @param _address tuple center address
  * @param _port tuple center port
  * @param _topic tuple center topic
  * @param _location tuple center location
  * @param _timestamp tuple center time stamp, used to clean DNSSERVER record
  */

class TupleCenter(_id: String, _name: String, var _address: String, _port: Int, var _topic: String, var _location: String, var _timestamp: Long) extends iTupleCenter {

  def this() = this("_", "_", "_", 0, "_", "_", -1)

  def this(name: String, address: String, port: Int, topic: String, location: String) = this(UUID.randomUUID().toString, name, address, port, topic, location, 0)

  def this(name: String, address: String, port: Int) = this(name, address, port, "unknown", "unknown")

  override def toJson = new Gson().toJson(new TupleCenter(id, name, address, port, topic, location, timestamp) /*this.asInstanceOf[TupleCenter]*/).stripMargin

  def name: String = _name

  def address: String = _address

  def address_=(address: String) {
    _address = address
  }

  def topic: String = _topic

  def topic_=(topic: String) {
    _topic = topic
  }

  def timestamp: Long = _timestamp

  def port: Int = _port

  def location: String = _location

  def location_=(location: String) {
    _location = location
  }

  override def id: String = _id

  def topicCriteria(topic: String): Boolean =
    if (this.topic.endsWith("*")) /* fuzzy topic */
      topic.equals("*") ||
        /* myTopic = *, yourTopic = whatever => OK */
        this.topic.equals("*") ||
        /* myTopic = a.*, yourTopic = a.b.* => OK */
        topic.startsWith(this.topic.substring(0, this.topic.length - 2)) ||
        /* myTopic = a.b.*, yourTopic = a.* => OK */
        this.topic.startsWith(topic.substring(0, topic.length - 2))
    else this.topic.equals(topic) /* specific topic */

  def locationCriteria(location: String): Boolean = this.location.equals(location)

  def neighborCriteria(topic: String, location: String): Boolean = topicCriteria(topic) || locationCriteria(location)

  def rejectOther(tc: iTupleCenter) = println(s"[$toString] rejected ${tc.toString}")

  def addOther(tc: iTupleCenter) = {}

  def addSelf(tc: iTupleCenter) = {}

  def updateTimeStamp(): Unit = _timestamp = System.currentTimeMillis

}

object TupleCenter {
  def fromJson(json: String): TupleCenter = new Gson().fromJson(json, new TypeToken[TupleCenter]() {}.getType)

  def fromJsonList(json: String): Array[TupleCenter] = new Gson().fromJson(json, new TypeToken[Array[TupleCenter]]() {}.getType)
}


