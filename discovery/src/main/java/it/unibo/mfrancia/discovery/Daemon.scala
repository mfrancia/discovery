package it.unibo.mfrancia.discovery


abstract class Daemon(_id: String) extends Thread {
  var s = false

  def id: String = _id

  def setStop(): Unit = {
    s = true
    interrupt
  }

  def isStopped = s
}
