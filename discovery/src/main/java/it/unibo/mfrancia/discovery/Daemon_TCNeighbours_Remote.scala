package it.unibo.mfrancia.discovery

import scala.collection.mutable


/**
 * Every "sleepMillis" get the list of neighbours
 * @param tc specified tuple center
 * @param sleepMillis
 */
class Daemon_TCNeighbours_Remote(tc: mutable.Buffer[iTupleCenter], sleepMillis: Long)
  extends Daemon_TCNeighbours("Daemon_TCNeighbours_Remote", tc) {
  val http = new MyHttp

  def this(sleepMillis: Long) = this(mutable.ListBuffer.empty, sleepMillis)

  override def run =
    while (!isStopped) {
      tc.foreach(t => t.addNeighbour(getNeighbours(t)))
      Thread.sleep(sleepMillis)
    }

  def getNeighbours(tc: iTupleCenter): Seq[iTupleCenter] = {
    /* get request of neighbours with specified parameters */
    val json = http.sendGet(Syskb.DNSSERVER_SEARCH(tc))
    /* response deserialization */
    val list: Seq[TupleCenter] = TupleCenter.fromJsonList(json)
    /* debug purpose */
    println(s"[Daemon_TCNeighbours_Remote] checking neigh for ${tc.toString}")
    list
  }
}
