package it.unibo.mfrancia.discovery

/**
 * Http local daemon. This daemon is linked to a specified tuple center.
 * If the tuple center is alive then ping the (DNS) server. This daemon is NOT
 * shutdown until the tuple center is not available after "n" retries
 */

class Daemon_TCPing_Remote(tc: iTupleCenter, millisPing: Long, retriesNumber: Int) extends Daemon_TCPing("ping_remote " + tc.toString, tc, millisPing, retriesNumber) {
  val http = new MyHttp[iTupleCenter](Syskb.DNSSERVER_TUPLECENTER)

  // sending tc into HTTP packet
  override def ping =
    tc.address = http.sendPost(Syskb.DNSSERVER_PING(tc.id), tc)

  //... or do I have to send a tuple center as a list of params?
  //tc.setId(Http(Syskb.PING_(tc.id)).params(tc.getFields()).asString.body)

  override def isTcAlive = true
}
