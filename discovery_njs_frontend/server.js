var http = require('http'),
    mongo = require('mongojs'),
    db = mongo('127.0.0.1:27017/tuplecenterdb', ['tuplecenters']);

http.createServer(function (req, res) {
    db.tuplecenters.find({}, function (err, tuplecenters) {
        if (err) {
            console.log(err.toString());
            throw err;
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<!DOCTYPE html><html><head><meta http-equiv="refresh" content="1" />' +
            '<style>#customers {font-family: "Trebuchet MS", Arial, ' +
            'Helvetica, sans-serif;width: 100%;border-collapse: collapse;}#customers td, #customers th {' +
            'font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;}#customers th {font-size: 1.1em;' +
            'text-align: left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942;color: #ffffff;}' +
            '#customers tr.alt td {color: #000000;background-color: #EAF2D3;}</style>' +
            '</head>' +
            '<body>' +
                //'function updateClock() { var now = new Date(),time = now.getHours() + ":" + now.getMinutes() + ":" + now.getMilliseconds(),' +
                //'date = [now.getDate(),months[now.getMonth()],now.getFullYear()].join(" ");' +
                //'document.getElementById("time").innerHTML = [date, time].join(" / ");' +
                //'setTimeout(updateClock, 1000);} updateClock();' +
                //'<div id="time"></div>
            '<table id="customers">');
        res.write('<table id="customers">');
        res.write('<tr>');
        res.write('<th>_id</th>');
        res.write('<th>name@address:port</th>');
        res.write('<th>topic</th>');
        res.write('<th>location</th>');
        res.write('<th>timestamp</th>');
        res.write('</tWr>');
        tuplecenters.forEach(function (tc) {
            res.write('<tr>');
            res.write('<td>' + tc._id + '</td>');
            res.write('<td>' + tc._name + '@' + tc._address + ':' + tc._port + '</td>');
            res.write('<td>' + tc._topic + '</td>');
            res.write('<td>' + tc._location + '</td>');
            res.write('<td>' + tc._timestamp + '</td>');
            res.write('</tr>');
        });
        res.end('</table></body></html>');
    });
}).listen(8081);
